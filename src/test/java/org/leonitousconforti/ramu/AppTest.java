package org.leonitousconforti.ramu;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.leonitousconforti.ramu.models.CatalogEntry;

/**
 * Unit test for simple App.
 */
public class AppTest {
    CatalogEntry ce;

    /**
     * Rigorous Test :-).
     */
    @Test
    public void shouldAnswerWithTrue() throws IOException, InterruptedException {
        final String testString = "Name=sampleApplication,Version=1.2.3,Description=this is a sample application,Type=Application,InstallerUri=https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/sample/sampleApplication-installer.sh,UninstallerUri=https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/sample/sampleApplication-uninstaller.sh,IconResourceUri=https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/sample/sameApplication-icon.png";
        ce = new CatalogEntry(testString);

        assertEquals("parsed name", "sampleApplication", ce.name);
        assertEquals("parsed version", "1.2.3", ce.version);
        assertEquals("parsed description", "this is a sample application", ce.description);
        assertEquals("parsed type", CatalogEntry.ModelTypes.Application, ce.type);
        assertEquals("parsed installed", false, ce.installed);
        assertEquals("parsed installerUri", "https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/sample/sampleApplication-installer.sh", ce.installerUri);
        assertEquals("parsed uninstallerUri", "https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/sample/sampleApplication-uninstaller.sh", ce.uninstallerUri);
        assertEquals("parsed IconResourceUri", "https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/sample/sameApplication-icon.png?inline=false", ce.iconResourceUri);
    }
}
