package org.leonitousconforti.ramu;

import de.milchreis.uibooster.*;
import de.milchreis.uibooster.components.*;
import de.milchreis.uibooster.model.*;

import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.*;

import org.leonitousconforti.ramu.models.*;

/**
 * Ramu is our Robotics Application Manager Utility - it has a remote catalog
 * which you can install apps, extensions, and patches from and automatically
 * handles updates of everything.
 */
public class App {

    // GUI forms and UI stuff
    private final Form mainForm;
    private final UiBooster uiBooster;

    // Catalog
    private String currentCatalogUrl;
    private Map<String, CatalogEntry> catalog;

    /**
     * Entry point of the program.
     *
     * @param args the args from the command line
     */
    public static void main(String[] args) {
        new App();
    }

    // Private constructor
    private App() {
        // UiBooster splash screen on launch
        uiBooster = new UiBooster(new UiBoosterOptions(UiBoosterOptions.Theme.DARK_THEME));

        // Run the startup sequence
        onStartup();

        // Create the main form and run it
        mainForm = uiBooster.createForm("Ramu");
        mainForm.addButton("view remote catalog", () -> onViewRemoteCatalog());
        mainForm.addButton("view installed", () -> onViewInstalledApplications());
        mainForm.addButton("change catalog branch", () -> onChangeRemoteCatalog());
        mainForm.addButton("view catalog branch", () -> onViewCatalogSelection());
        mainForm.addButton("refresh catalog", () -> onRefreshCatalog());
        mainForm.addButton("Quit program", () -> onQuit());
        mainForm.addLabel("Ramu Version: v0.0.1, released: 1/8/21");
        mainForm.addLabel("@leonitous");
        mainForm.run();
    }

    /**
     * Pulls our remote catalog from the web and creates a new map with their names
     * as the keys. Also stores a local copy of the remote catalog
     *
     * @param remoteCatalogUrl   the uri of the remote catalog
     * @param pathToLocalCatalog the path to the local catalog
     * @throws IOException no internet?
     */
    private Map<String, CatalogEntry> pullRemoteCatalog(String remoteCatalogUrl, Path pathToLocalCatalog)
            throws IOException {
        // Make a url
        URL url = new URL(remoteCatalogUrl);

        // Readable byte channel
        ReadableByteChannel rbc = Channels.newChannel(url.openConnection().getInputStream());

        // Make sure out config file exists
        pathToLocalCatalog.toFile().getParentFile().mkdirs();
        pathToLocalCatalog.toFile().createNewFile();

        // Update the local file
        FileOutputStream fos = new FileOutputStream(pathToLocalCatalog.toFile().getAbsolutePath());
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

        // Read the local file that was just updated and create a map with it
        return Files.lines(pathToLocalCatalog, StandardCharsets.UTF_8).map(string -> new CatalogEntry(string))
                .collect(Collectors.toMap(model -> model.name, model -> model));
    }

    /**
     * Shorthand for {@link #pullRemoteCatalog(String, Path)}.
     *
     * @throws IOException no internet?
     */
    private void pullRemoteCatalog() throws IOException {
        catalog = pullRemoteCatalog(currentCatalogUrl, Config.persistentLocalCatalogPath);
    }

    /**
     * Saves all the installed app versions to a persistent file in the users home
     * directory.
     *
     * @param pathToConfig the path to the config file
     * @param versionTable The list of apps and their installed version
     * @throws IOException if the system can not find the config file
     */
    private void writeAppVersionsToDisk(Path pathToConfig, Map<String, String> versionTable) throws IOException {
        // Make sure out config file exists
        pathToConfig.toFile().getParentFile().mkdirs();
        pathToConfig.toFile().createNewFile();

        // Make a file Writer
        PrintWriter fileWriter = new PrintWriter(pathToConfig.toFile(), StandardCharsets.UTF_8);

        // For every app that has an entry, concat its name with its version
        for (String appName : versionTable.keySet().toArray(String[]::new)) {
            fileWriter.println(appName + Config.versionConfigStringSplitter + versionTable.get(appName));
        }

        // Prevent resource leaks by closing the writer
        fileWriter.close();
    }

    /**
     * Reads all the installed app versions form a persistent file in the users home
     * directory.
     *
     * @param pathToConfig the path to the config file
     * @throws IOException if the system can not find the config file
     */
    private Map<String, String> fetchAppVersionsFromDisk(Path pathToConfig) throws IOException {
        // Make sure out config file exists
        pathToConfig.toFile().getParentFile().mkdirs();
        pathToConfig.toFile().createNewFile();

        // Shorthand this name
        final String vcss = Config.versionConfigStringSplitter;

        // Read the file and format it to a nice map of the apps name and the version
        return Files.lines(pathToConfig, StandardCharsets.UTF_8)
                .collect(Collectors.toMap(s -> s.split(vcss)[0], s -> s.split(vcss)[1]));
    }

    /**
     * Resources located inside the jar, which is compressed, can not be read
     * directly. Best option I found was to copy to copy the file to a temp file and
     * load that, the temp file deletes itself when the program closes
     *
     * @param prefix the name of the file to load
     * @param suffix the file type of the file
     * @throws IOException bad file name
     */
    private File loadJarResource(String prefix, String suffix) throws IOException {
        // Get the class loader for loading the html file from the jar resources
        ClassLoader classLoader = getClass().getClassLoader();

        // Buffer stream for reading the contents of the resource file
        InputStream in = classLoader.getResourceAsStream(prefix.concat(suffix));

        // Create a temp file and delete it on program exit
        File temp = File.createTempFile(prefix, suffix, null);
        temp.deleteOnExit();

        // Copy the resource file to the temp file
        Files.copy(in, temp.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        return temp.getAbsoluteFile();
    }

    // ======================================== //
    // ======================================== //
    // ============= UI Methods =============== //
    // ======================================== //
    // ======================================== //

    /**
     * Displays an exception to the user and prompt to continue or not.
     *
     * @param when  when during the app did this exception occur
     * @param fatal if fatal error, the user will not be prompted to continue
     * @param e     the exception message
     */
    @SuppressWarnings("checkstyle:Indentation")
    private void displayException(String when, boolean fatal, Exception e) {
        // Alert the user
        uiBooster.showErrorDialog("An error has ocurred", "ERROR");

        // If there was an exception, show it
        final String _fatal = fatal ? "fatal" : "non-fatal";
        uiBooster.showException("A " + _fatal + " error has ocurred " + when, "Exception Message", e);

        // Prompt the user to continue- i.e it would be fine to continue the application
        // if it just couldn't pull the remote catalog because of an internet issue.
        if (!fatal) {
            uiBooster.showConfirmDialog("Would you like to continue anyways?", "Continue?",
                    () -> uiBooster.showWarningDialog("Continuing can have unintended side-effects!", "WARN"),
                    () -> System.exit(-1));
        } else if (fatal) {
            System.exit(-1);
        }
    }

    /**
     * Runs the apps startup sequence of checking into the remote repository and
     * downloading new content.
     *
     * @see: {@link #onQuit()}
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    private void onStartup() {
        currentCatalogUrl = Config.stableRemoteCatalogUrl;

        try {
            // Attempt to load the splash picture and display it, this can fail for if the
            // picture does not exists as a resource or an io exception occurs
            File splash = loadJarResource("splash", ".png");
            uiBooster.showSplashscreen(splash.getAbsolutePath(), 7000);

            // Attempt to pull the remote catalog, this can fail if the catalog does not
            // exist in the repo or if the filesystem can not save it locally.
            pullRemoteCatalog();

            // Set the ones we have installed
            fetchAppVersionsFromDisk(Config.persistentVersionConfigPath).keySet()
                    .forEach(key -> catalog.get(key).installed = true);
        } catch (Exception e) {
            displayException("while launching the app", false, e);
        }

        // Wait
        try {
            // Wait before continuing for the splash screen to go away
            TimeUnit.MILLISECONDS.sleep(7000);
        } catch (InterruptedException e) {
            displayException("while launching the app", false, e);
        }
    }

    /**
     * Saves modified data to persistent local storage when the application closes.
     *
     * @see: {@link #onStartup()}
     */
    private void onQuit() {
        // Save to persistent storage when application closes
        try {
            // Filter the catalog by everything that is currently installed
            Map<String, String> formattedCatalog = catalog.keySet().stream().map(key -> catalog.get(key))
                    .filter(entry -> entry.installed)
                    .collect(Collectors.toMap(entry -> entry.name, entry -> entry.version));

            // Update the local storage
            writeAppVersionsToDisk(Config.persistentVersionConfigPath, formattedCatalog);
        } catch (Exception e) {
            displayException("while saving app versions to local persistent storage", true, e);
        }

        // Safe shutdown
        System.exit(0);
    }

    /**
     * Crafts the catalog entry into a List element to be used in the gui.
     */
    private ListElement catalogEntryToListElement(CatalogEntry entry) {
        // Craft the header
        String header = entry.formatHeader();

        // Fetch the icon resource
        try {
            File icon = entry.downloadRemoteResource(entry.iconResourceUri, ".png");

            // Make the list element with the icon
            return new ListElement(header, entry.description, icon.getAbsolutePath());
        } catch (Exception e) {
            displayException("while downloading icon for " + entry.name, false, e);

            // Make the list element without the icon
            return new ListElement(header, entry.description);
        }
    }

    /**
     * Wrapper method for viewing the installed applications from the main form.
     */
    private void onViewInstalledApplications() {
        // Try to fetch the local versions file
        try {
            ListElement[] localInstalledApps;

            // Fetch
            localInstalledApps = fetchAppVersionsFromDisk(Config.persistentVersionConfigPath).entrySet().stream()
                    .map(entry -> catalogEntryToListElement(catalog.get(entry.getKey()))).toArray(ListElement[]::new);

            // Show the results
            uiBooster.showList("all installed ", "Installed Applications", localInstalledApps);
        } catch (Exception e) {
            // Log the exception
            displayException("while fetching app versions from disk", false, e);
        }
    }

    /**
     * Wrapper method for viewing the remote catalog.
     */
    private void onViewRemoteCatalog() {
        try {
            // Fetch the most recent remote catalog
            pullRemoteCatalog();

            // Set the ones we have installed
            fetchAppVersionsFromDisk(Config.persistentVersionConfigPath).keySet()
                    .forEach(key -> catalog.get(key).installed = true);

            // Parse it
            ListElement[] catalogItems = catalog.entrySet().stream()
                    .map(entry -> catalogEntryToListElement(entry.getValue())).toArray(ListElement[]::new);

            // Render it
            uiBooster.showList("Choose an app", "Choose and app from the catalog to install, press \"ok\" to continue",
                    this::onSelectRemoteCatalogEntry, catalogItems);
        } catch (Exception e) {
            // Log the exception
            displayException("while viewing/downloading the remote catalog", false, e);
        }
    }

    /**
     * Wrapper method for changing the remote catalog.
     */
    @SuppressWarnings("checkstyle:RightCurly")
    private void onChangeRemoteCatalog() {
        // Prompt for a remote catalog
        String catalogSelection = uiBooster.showSelectionDialog("What catalog would you like to use?",
                "Choose a remote catalog", Arrays.asList("stable", "nightly", "custom"));

        // Set the stable catalog
        if (catalogSelection.equals("stable")) {
            currentCatalogUrl = Config.stableRemoteCatalogUrl;
        }

        // Set the nightly catalog
        else if (catalogSelection.equals("nightly")) {
            currentCatalogUrl = Config.nightlyRemoteCatalogUrl;
        }

        // Prompt for a custom catalog
        else if (catalogSelection.equals("custom")) {
            String custom = uiBooster.showTextInputDialog("Enter the url of your custom remote catalog");

            // If the user cancels the prompt, the returned string will be null
            if (!custom.equals(null)) {
                currentCatalogUrl = custom;
            }
        }
    }

    /**
     * Wrapper method for viewing the current remote catalog selection.
     */
    private void onViewCatalogSelection() {
        uiBooster.showInfoDialog("The current remote catalog uri is: " + currentCatalogUrl);
    }

    /**
     * Wrapper method for refreshing the catalog.
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    private void onRefreshCatalog() {
        // Do the work
        try {
            pullRemoteCatalog();
        } catch (Exception e) {
            // Log the exception
            displayException("while refreshing the remote catalog", false, e);
        }
    }

    /**
     * Attempts to run the installer or uninstaller for an application when it is
     * selected.
     *
     * @param element the element on the list
     */
    @SuppressWarnings("checkstyle:RightCurly")
    private void onSelectRemoteCatalogEntry(ListElement element) {
        // Parse the name from its header
        final String applicationName = element.getTitle().split("@")[0];

        // Prompt the user to install or uninstall
        String selection = uiBooster.showSelectionDialog("Would you like to uninstall or install " + element.getTitle(),
                "Action required", Arrays.asList("install", "uninstall", "neither!"));

        // Take the user back
        if (selection.equals("neither")) {
            return;
        }

        // Install
        if (selection.equals("install")) {
            // Check to see if it is already installed
            if (catalog.get(applicationName).installed) {
                uiBooster.showInfoDialog(element.getTitle()
                        + " is already installed, please uninstall it if you would like to reinstall it");
                return;
            }

            try {
                // try to install it
                catalog.get(applicationName).install();

                // Filter the catalog by everything that is currently installed
                Map<String, String> formattedCatalog = catalog.keySet().stream().map(key -> catalog.get(key))
                        .filter(entry -> entry.installed)
                        .collect(Collectors.toMap(entry -> entry.name, entry -> entry.version));

                // Update the catalog
                writeAppVersionsToDisk(Config.persistentVersionConfigPath, formattedCatalog);
            } catch (Exception e) {
                // Log the exception
                displayException("while running the installer script for: " + element.getTitle(), true, e);
            }
        }

        // Uninstall
        else if (selection.equals("uninstall")) {
            // Check to see if it is not installed yet
            if (!catalog.get(applicationName).installed) {
                uiBooster.showInfoDialog(element.getTitle() + " is not installed yet, can not uninstall it");
                return;
            }

            try {
                // try to uninstall it
                catalog.get(applicationName).uninstall();

                // Filter the catalog by everything that is currently installed
                Map<String, String> formattedCatalog = catalog.keySet().stream().map(key -> catalog.get(key))
                        .filter(entry -> entry.installed)
                        .collect(Collectors.toMap(entry -> entry.name, entry -> entry.version));

                // Update the catalog
                writeAppVersionsToDisk(Config.persistentVersionConfigPath, formattedCatalog);
            } catch (Exception e) {
                // Log the exception
                displayException("while running the uninstaller script for: " + element.getTitle(), true, e);
            }
        }
    }
}
