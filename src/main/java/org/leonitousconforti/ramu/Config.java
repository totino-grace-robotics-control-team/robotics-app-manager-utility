package org.leonitousconforti.ramu;

import java.nio.file.*;

@SuppressWarnings("checkstyle:SummaryJavadoc")
/**
 * DO NOT MESS WITH THESE VALUES UNLESS YOU KNOW WHAT YOU ARE DOING!
 */
public final class Config {
    // Our config file for app versions
    public static final Path persistentVersionConfigPath = Paths.get(System.getProperty("user.home"), ".ramu",
            "installedAppVersions.txt");

    // Our config file for the local catalog
    public static final Path persistentLocalCatalogPath = Paths.get(System.getProperty("user.home"), ".ramu",
            "localCatalog.txt");

    // Remote catalog url
    public static final String stableRemoteCatalogUrl = "https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/catalog-stable.txt";
    public static final String nightlyRemoteCatalogUrl = "https://gitlab.com/totino-grace-robotics-control-team/robotics-app-manager-utility/-/raw/master/src/catalog/catalog-nightly.txt";

    // Splitter for the versions and catalog strings
    public static final String versionConfigStringSplitter = "@";
    public static final String catalogEntryPropertyDelimiter = ":";
}
