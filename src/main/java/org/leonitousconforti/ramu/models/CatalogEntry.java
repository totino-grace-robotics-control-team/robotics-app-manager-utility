package org.leonitousconforti.ramu.models;

import java.io.*;
import java.net.*;
import java.nio.channels.*;
import java.util.*;
import java.util.stream.*;

/**
 * A base model - can represent an Application, Extension, or a Patch.
 */
public class CatalogEntry {

    // Types enum
    public static enum ModelTypes {
        Application, Extension, Patch, None
    }

    // Name, version, and type of the model
    public final String name;
    public final String version;
    public final ModelTypes type;
    public final String description;
    public boolean installed = false;

    // Uri's to external model resources
    public final String installerUri;
    public final String uninstallerUri;
    public final String iconResourceUri;

    /**
     * Creates a new baseModel using regex's (Regular Expressions) to parse the
     * elements from the catalog list.
     *
     * @param catalogEntry the string parsed from the catalog
     */
    public CatalogEntry(String catalogEntry) {
        // Parse properties
        Map<String, String> properties = Arrays.stream(catalogEntry.split(",")).map(property -> property.split("="))
                .collect(Collectors.toMap(propArr -> propArr[0], propArr -> propArr[1]));

        // Set name, version, and description
        name = properties.get("Name");
        version = properties.get("Version");
        description = properties.get("Description");

        // Set the type
        if (Arrays.stream(ModelTypes.values()).filter(t -> t.name().equals(properties.get("Type"))).count() > 0) {
            type = ModelTypes.valueOf(properties.get("Type"));
        } else {
            type = ModelTypes.None;
        }

        // Set resource URI's
        installerUri = properties.get("InstallerUri");
        uninstallerUri = properties.get("UninstallerUri");
        iconResourceUri = properties.get("IconResourceUri") + "?inline=false";
    }

    public String formatHeader() {
        return name + "@" + version + ":" + (installed ? "installed" : "not installed") + "-" + type.name();
    }

    /**
     * Attempts to download a remote resource.
     *
     * @see: https://stackoverflow.com/questions/921262/how-to-download-and-save-a-file-from-internet-using-java
     * @param urlOfResource the url of the remote resource
     * @param suffix        the suffix of the file
     * @throws IOException no internet?
     */
    public File downloadRemoteResource(String urlOfResource, String suffix) throws IOException {
        // Make a url
        URL url = new URL(urlOfResource);

        // Readable byte channel
        ReadableByteChannel rbc = Channels.newChannel(url.openConnection().getInputStream());

        // Create a temp file and delete it on program exit
        File temp = File.createTempFile("ramuDownload", suffix, null);
        temp.deleteOnExit();

        // Write to the temp file
        FileOutputStream fos = new FileOutputStream(temp.getAbsolutePath());
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

        // Done!
        return temp.getAbsoluteFile();
    }

    /**
     * Runs the installer script for this model, opposite of {@link #uninstall()}.
     *
     * @throws IOException          no internet?
     * @throws InterruptedException interrupted while running the script
     */
    public void install() throws IOException, InterruptedException {
        // Download the installer and run it
        File installScript = downloadRemoteResource(installerUri, ".sh");
        runScript(installScript);
        installed = true;
    }

    /**
     * Runs the uninstaller script for this model, opposite of {@link #install()}.
     *
     * @throws IOException          no internet?
     * @throws InterruptedException interrupted while running the script
     */
    public void uninstall() throws IOException, InterruptedException {
        // Download the uninstaller and run it
        File uninstallerScript = downloadRemoteResource(uninstallerUri, ".sh");
        runScript(uninstallerScript);
        installed = false;
    }

    /**
     * Runs a shell script.
     *
     * @see: https://stackoverflow.com/questions/525212/how-to-run-unix-shell-script-from-java-code
     * @see: https://stackoverflow.com/questions/53113127/java-runtime-exec-fails-to-run/55091040
     * @param script the script file to run
     * @throws IOException          file system error
     * @throws InterruptedException interrupted while running the script
     */
    private int runScript(File script) throws IOException, InterruptedException {
        // Make script executable
        script.setExecutable(true);

        // Run the script with the process builder
        ProcessBuilder pb = new ProcessBuilder("sh", script.getAbsolutePath());
        int processReturnCode = pb.start().waitFor();
        return processReturnCode;
    }
}
