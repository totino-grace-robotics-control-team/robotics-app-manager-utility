-- Remove any previous attempts/installs
-- do shell script "rm -f ~/Downloads/openjdk-15.0.1_osx-x64_bin.tar.gz && rm -rf ~/Library/Printers/jdk-15.0.1.jdk"

-- Download open jdk to the users download directory
-- do shell script "curl -C - https://download.java.net/java/GA/jdk15.0.1/51f4f36ad4ef43e39d0dfdbaf6549e32/9/GPL/openjdk-15.0.1_osx-x64_bin.tar.gz -o ~/Downloads/openjdk-15.0.1_osx-x64_bin.tar.gz"

-- Unzip the jdk tar to the printers directory
-- do shell script "tar xf ~/Downloads/openjdk-15.0.1_osx-x64_bin.tar.gz -C ~/Library/Printers"

-- Remove the tar from the users download folder
-- do shell script "rm -f ~/Downloads/openjdk-15.0.1_osx-x64_bin.tar.gz"

-- Update path variables
-- do shell script "mv ~/.zprofile ~/.zprofile-backup"
-- do shell script "touch ~/.zprofile"
-- do shell script "echo \"export PATH=~/Library/Printers/jdk-15.0.1.jdk/Contents/Home/bin:$PATH\" >> ~/.zprofile"
-- do shell script "echo \"export JAVA_HOME=~/Library/Printers/jdk-15.0.1.jdk/Contents/Home\" >> ~/.zprofile"

-- Install Ramu
do shell script "rm -rf ~/Library/Printers/ramu.app"
do shell script "cp -r /Volumes/ramu/ramu.app ~/Library/Printers/ramu.app"
do shell script "/usr/local/bin/dockutil --add ~/Library/Printers/ramu.app --replacing 'ramu'"
