#!/bin/bash

# Bash "strict mode", to help catch problems and bugs in the shell
# script. Every bash script you write should include this. See
# http://redsymbol.net/articles/unofficial-bash-strict-mode/ for
# details.
set -euo pipefail

rm -f ~/Downloads/processing-3.5.4-macosx.zip
curl -L -C - https://github.com/processing/processing/releases/download/processing-0270-3.5.4/processing-3.5.4-macosx.zip -o ~/Downloads/processing-3.5.4-macosx.zip
mkdir -p ~/Library/Printers/processing-3.5.4
unzip -q -o ~/Downloads/processing-3.5.4-macosx.zip -d ~/Library/Printers/processing-3.5.4
rm -f ~/Downloads/processing-3.5.4-macosx.zip
/usr/local/bin/dockutil --add ~/Library/Printers/processing-3.5.4/Processing.app --replacing 'Processing'
