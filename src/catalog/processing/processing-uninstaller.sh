#!/bin/bash

# Bash "strict mode", to help catch problems and bugs in the shell
# script. Every bash script you write should include this. See
# http://redsymbol.net/articles/unofficial-bash-strict-mode/ for
# details.
set -euo pipefail

rm -rf ~/Library/Printers/processing-3.5.4
/usr/local/bin/dockutil --remove 'Processing'
