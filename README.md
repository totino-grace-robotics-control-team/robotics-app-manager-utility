# Robotics App Manager Utility

![logo](screenshots/logo.png)

Ramu is a utility to easily install dev tools and other programming environments onto your school laptops. This is a standalone application that you only download once. There is a remote catalog, managed by yours truly, which will be updated with new apps and such as I get them to work on our school laptops. If you have a suggestions for an app you would like to see here, shoot me an email and I will add it as soon as I can.

**Note: new updates will often be found in the _nightly_ catalog first, and since they will have frequent changes made to them you can think of them in an alpha stage. If you are looking for stable versions, make sure you are using the _stable_ catalog**

## Switching repositories

There are two different repositories. The 'stable' repository offers applications and extensions which are tested and guaranteed to work. The 'nightly' repository offers the most recent updates and apps, but some may be untested or not quite working yet. Use the repository switcher from the main menu to choose which catalog you would like to use.

![Repository switcher](screenshots/repository_switcher.png)
